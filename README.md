# uniapp_swiper [![star](https://gitee.com/suixinxingyanghao/uniapp_swiper/badge/star.svg?theme=dark)](https://gitee.com/suixinxingyanghao/uniapp_swiper)
## 如果对你有用希望点一下 有上角的star 谢谢

#### 介绍
##### uniapp_swiper  轮播图使用


##### 目前需求是：
轮播图滚动过程中的时候，显示的图片变大，并且需要渐变。 在改变的过程中背景图片也改变

#### 效果
![效果](https://images.gitee.com/uploads/images/2021/0802/104602_5b11cd04_5292673.gif "uniapp_swiper.gif")


##### 下载：`git https://gitee.com/suixinxingyanghao/uniapp_swiper.git`

##### 下载完成：
![下载完成](https://images.gitee.com/uploads/images/2021/0802/100621_a77a52c7_5292673.png "屏幕截图.png")
##### 打开这个：
![打开这个](https://images.gitee.com/uploads/images/2021/0802/100657_53f27076_5292673.png "屏幕截图.png")
##### HBuilerX 打开可直接运行。
